import urllib
import os
import json
import base64
import time
from dateutil import parser
from dateutil import tz
import traceback
from urlparse import urlparse, parse_qs
import requests
import datetime

import StorageServer

import xbmcplugin
import xbmcgui
import xbmcaddon
import xbmcvfs

addon = xbmcaddon.Addon()
addon_version = addon.getAddonInfo("version")
addon_id = addon.getAddonInfo("id")
addon_path = xbmc.translatePath(addon.getAddonInfo("path"))
addon_profile = xbmc.translatePath(addon.getAddonInfo("profile"))
icon = os.path.join(addon_path, "resources", "4x3_icon.png")
fanart = addon.getAddonInfo("fanart")
cache = StorageServer.StorageServer("blazetv", 24)
base_url = "http://www.blazetv.com"
api_url = "https://api.unreel.me/v2/sites/blazetv/"
language = addon.getLocalizedString
auth_token = None


def addon_log(string):
    try:
        log_message = string.encode("utf-8", "ignore")
    except:
        log_message = "addonException: addon_log: {0}".format(traceback.format_exc())
        xbmc.log(os.linesep.join(traceback.format_stack()))
    xbmc.log(
        "[{0}-{1}]: {2}".format(addon_id, addon_version, log_message),
        level=xbmc.LOGDEBUG,
    )


def make_request(url, data=None, headers=None):
    addon_log("Request URL: {0}".format(url))
    if headers is None:
        headers = {}
    if "User-agent" not in headers:
        headers["User-agent"] = "Kodi/BlazeTV Addon"
    auth_token = do_login()

    if auth_token is not None and "token" in auth_token:
        headers["Authorization"] = "Bearer " + auth_token["token"]

    try:
        response = requests.get(url, data=data, headers=headers)
        data = response.json()
        if len(response.history) > 0:
            for h in response.history:
                addon_log("Redirect URL: %s" % h)
    except (requests.ConnectionError, Exception) as e:
        if not isinstance(e, requests.ConnectionError):
            addon_log("Unknown error: %s" % type(e))
        addon_log('We failed to open "%s".' % url)
        if hasattr(e, "reason"):
            addon_log("We failed to reach a server.")
            addon_log("Reason: " + str(e.reason))
        elif hasattr(e, "code"):
            addon_log("We failed with error code - %s." % e.code)
        else:
            addon_log(str(e))
        data = None
    return data


def cache_shows():
    data = make_request(api_url + "channels/series/series")
    shows = []
    for i in data["items"]:
        shows.append({"name": i["title"], "thumb": i["poster"], "url": i["uid"]})
    return shows


def display_shows():
    addon_log("Displaying shows")
    if do_login() is not None:
        addon_log("Adding live")
        add_dir(language(30013), "live_shows", icon, "get_live_shows")
    data = cache.cacheFunction(cache_shows)
    for i in data:
        add_dir(i["name"], i["url"], i["thumb"], "get_show")


def add_display_show(jsonData):
    for i in jsonData:
        if i is None:
            pass
        elif isinstance(i, list):
            add_display_show(i)
        elif (
            "accessType" in i
            and i["accessType"] == "requires login"
            and addon.getSetting("prem_content") == "true"
        ):
            add_dir(
                i["title"],
                i["uid"],
                i["metadata"]["thumbnails"]["maxres"],
                "resolve_url",
                info=i,
            )
        elif "accessType" not in i or i["accessType"] != "requires login":
            add_dir(
                i["title"],
                i["uid"],
                i["metadata"]["thumbnails"]["maxres"],
                "resolve_url",
                info=i,
            )


def display_show(show_url):
    episodes = parse_video_search(api_url + "series/" + show_url + "/episodes")
    cache.set("episodes_dict", repr({"episodes": episodes}))
    add_dir(language(30014), "get_cached_episodes", icon, "get_show_list")


def display_live_shows(date_format=None):
    videos = parse_video_search(api_url + "channels/live/live-events")
    for i in videos:
        time = parser.parse(i[0].pop("startBroadcast", None))
        if time < datetime.datetime.utcnow().replace(tzinfo=time.tzinfo):
            time = "LIVE"
        else:
            time = time.astimezone(tz=tz.tzlocal()).strftime("%I:%M %p")
        add_dir(
            time + ": " + i[0]["title"], i[1], i[2], "resolve_episode_url", i[0], i[3]
        )


def add_display_show(jsonData):
    for i in jsonData:
        if i is None:
            pass
        elif isinstance(i, list):
            add_display_show(i)
        elif (
            "accessType" in i
            and i["accessType"] == "requires login"
            and addon.getSetting("prem_content") == "true"
        ):
            add_dir(
                i["title"],
                i["uid"],
                i["metadata"]["thumbnails"]["maxres"],
                "resolve_url",
                info=i,
            )
        elif "accessType" not in i or i["accessType"] != "requires login":
            add_dir(
                i["title"],
                i["uid"],
                i["metadata"]["thumbnails"]["maxres"],
                "resolve_url",
                info=i,
            )


def actual_video_parse(videos, jsonData):
    for i in jsonData:
        if i is None:
            pass
        elif isinstance(i, list):
            actual_video_parse(videos, i)
        else:
            requiresLogin = "accessType" in i and i["accessType"] == "requiresLogin"
            info = {}
            item_ids = {}
            thumb = (
                i["metadata"]["thumbnails"]["maxres"]
                if "metadata" in i
                and "thumbnails" in i["metadata"]
                and "maxres" in i["metadata"]["thumbnails"]
                else None
            )
            url = i["uid"]

            if "series" in i:
                if "episode" in i["series"]:
                    info["episode"] = i["series"]["episode"]
                if "season" in i["series"]:
                    info["season"] = i["series"]["season"]
                info["mediatype"] = "episode"
            if "type" in i and i["type"] == "liveEvent" and "start" in i:
                info["startBroadcast"] = i["start"]
            if "tags" in i:
                info["tag"] = i["tags"]
            if "createdAt" in i:
                info["premiered"] = i["createdAt"]
            info["title"] = i["title"] if "title" in i else i["uid"]
            if "description" in i:
                info["plot"] = i["description"]
            if "contentDetails" in i and "duration" in i["contentDetails"]:
                info["duration"] = int(i["contentDetails"]["duration"])
            # TODO what is this for?
            if "featureContext" in i:
                item_ids["content"] = i["contentId"]
                for event in i["keywords"]:
                    if event["type"] == "calendar_event_id":
                        item_ids["event"] = event["keyword"]
                        break

            if (
                not requiresLogin
                or requiresLogin
                and addon.getSetting("prem_content") == "true"
            ):
                videos.append((info, url, thumb, item_ids))


def parse_video_search(search_url):
    data = make_request(search_url)
    videos = []
    if "items" in data:
        actual_video_parse(videos, data["items"])
    else:
        actual_video_parse(videos, data)
    return videos


def display_show_list(name):
    if "highlights" in name:
        for i in eval(cache.get("highlights_dict"))["highlights"]:
            add_dir(i[0]["title"], i[1], i[2], "resolve_episode_url", i[0])
    else:
        for i in eval(cache.get("episodes_dict"))["episodes"]:
            add_dir(i[0]["title"], i[1], i[2], "resolve_episode_url", i[0], i[3])


def do_login():
    # Check if tokens have expired. TODO
    global auth_token
    if (
        isinstance(auth_token, dict)
        and "token" in auth_token
        and "expiresIn" in auth_token
        and "refreshToken" in auth_token
        and False
    ):
        return auth_token
    else:
        addon_log("Login to get token!")
        login_url = "https://api.unreel.me/v2/me/auth/login?__site=blazetv"
        headers = {
            "User-agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0",
            "Content-Type": "application/json;charset=utf-8",
        }
        values = {
            "site": "blazetv",
            "email": addon.getSetting("email"),
            "password": addon.getSetting("password"),
        }
        r = requests.post(login_url, data=values)

        auth_token = r.json()

        if (
            "token" in auth_token
            and "expiresIn" in auth_token
            and "refreshToken" in auth_token
        ):
            addon_log("Logged in successfully!")
            return auth_token
        else:
            try:
                addon_log("html <title> : %s" % str(auth_token))
            except:
                addon_log(format_exc())
                pass
            addon_log("Login Failed")
            addon_log("We did not recive the required auth_token!")
            addon_log(r.text)
            notify(language(30017))


def resolve_prem_url(url, retry=False):
    addon_log(str(url))
    token = do_login()
    if token is None:
        addon_log("There seems to be an auth problem")
        return

    headers = {
        "User-agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0",
        "Referer": "http://www.video.theblaze.com/shared/flash/mediaplayer/v4.4/R10/MediaPlayer4.swf",  # TODO replace referer
        "Authorization": "Bearer " + token["token"],
    }
    data = make_request(api_url + "videos/" + url + "/play-url", headers=headers)
    if "url" in data:
        return data["url"]
    addon_log("Response Data: %s" % str(data))


def set_resolved_url(resolved_url):
    import inputstreamhelper  # pylint: disable=import-outside-toplevel

    success = False
    if resolved_url:
        success = True
    else:
        resolved_url = ""
    TYPE = "hls"  # TODO get this in resolve_prem_url
    item = xbmcgui.ListItem(path=resolved_url)
    inputStreamHelper = inputstreamhelper.Helper(TYPE)
    if inputStreamHelper.check_inputstream():
        play_item = xbmcgui.ListItem(path=resolved_url)
        play_item.setContentLookup(False)
        play_item.setMimeType("video/MP2T")  # TODO better?
        if int(xbmc.getInfoLabel("System.BuildVersion").split(".")[0]) >= 19:
            play_item.setProperty("inputstream", inputStreamHelper.inputstream_addon)
        else:
            play_item.setProperty(
                "inputstreamaddon", inputStreamHelper.inputstream_addon
            )
        play_item.setProperty("inputstream.adaptive.manifest_type", TYPE)
        xbmcplugin.setResolvedUrl(int(sys.argv[1]), success, play_item)
    else:
        xbmcplugin.setResolvedUrl(int(sys.argv[1]), success, item)


def notify(message):
    xbmc.executebuiltin(
        "XBMC.Notification(%s, %s, 10000, %s)" % ("Addon Notification", message, icon)
    )


def get_params():
    p = parse_qs(sys.argv[2][1:])
    for i in list(p.keys()):
        p[i] = p[i][0]
    return p


def add_dir(name, url, iconimage, mode, info={}, ids={}):
    isfolder = True
    params = {"name": name.encode("utf-8"), "url": url, "mode": mode, "ids": ids}
    addon_log(repr(params))
    url = "%s?%s" % (sys.argv[0], urllib.urlencode(params))
    listitem = xbmcgui.ListItem(
        name, iconImage="DefaultFolder.png", thumbnailImage=iconimage
    )
    listitem.setProperty("Fanart_Image", fanart)
    if mode in ["resolve_url", "resolve_episode_url", "pass"]:
        isfolder = False
        if mode != "pass":
            listitem.setProperty("IsPlayable", "true")
    listitem.setInfo("video", infoLabels=info)
    xbmcplugin.addDirectoryItem(int(sys.argv[1]), url, listitem, isfolder)


params = get_params()
addon_log(repr(params))
try:
    mode = params["mode"]
except:
    mode = None

if mode == None:
    display_shows()
    xbmcplugin.setContent(int(sys.argv[1]), "tvshows")
    xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode == "get_show":
    display_show(params["url"])
    xbmcplugin.setContent(int(sys.argv[1]), "episodes")
    xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode == "get_show_list":
    display_show_list(params["url"])
    xbmcplugin.setContent(int(sys.argv[1]), "episodes")
    xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode == "get_live_shows":
    display_live_shows()
    xbmcplugin.setContent(int(sys.argv[1]), "episodes")
    xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode == "select_calendar":
    select_calendar()
    xbmcplugin.setContent(int(sys.argv[1]), "episodes")
    xbmcplugin.endOfDirectory(int(sys.argv[1]))

elif mode == "resolve_episode_url":
    set_resolved_url(resolve_prem_url(params["url"]))

elif mode == "resolve_url":
    set_resolved_url(resolve_url(params["url"]))

elif mode == "pass":
    pass
